Draw go boards

Demo site: https://adamwight.gitlab.io/goban/

Running
===
Rebuild styles (add `-w` to watch continually):

  yarn run tailwindcss build -i input.css -o public/output.css

Host locally:

  python3 -m http.server --directory public/

Example conversion to pdf:

  inkscape -D --export-filename=goban-19.pdf goban-19.svg

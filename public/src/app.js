import { goban } from './goban.js'
import { SVG, find } from '../contrib/svg-3.2.0.esm.min.js'

// https://en.wikipedia.org/wiki/Go_equipment
// https://senseis.xmp.net/?DifferentSizedBoards

// Units are in mm
const default_config = {
	line_width: 1,
	star_point_diameter: 4,
	margin: 14,
}

const inkscape_dpi = 96
const mm_per_in = 25.4

function build_parameters(config) {
	config.size = parseInt(document.querySelector('input[name="count"]:checked').value)

	switch (document.querySelector('input[name="square"]:checked').value) {
		case 'ja':
			config.cell_width = 22
			config.cell_height = 23.7
			break
		case 'zh':
			config.cell_width = 23
			config.cell_height = 24
			break
		case 'square':
			config.cell_width = 23
			config.cell_height = 23
			break
		default:
			throw Error("Unknown aspect ratio")
	}

	return config
}

function render() {
	const config = build_parameters(default_config)

	const old = find('#drawing')
	if ( old.length > 0 ) {
		old.remove()
	}

	// TODO: set up accurate margins
	const container = document.getElementById('draw-container')
	container.dataset.filename = `goban-${config.size}.svg`
	const scale = inkscape_dpi / mm_per_in
	const canvas = SVG().addTo(container)
	  .size("100%", "100%")
		.viewbox(
			-config.margin,
			-config.margin,
			config.cell_width * (config.size - 1) + config.margin,
			config.cell_height * (config.size - 1) + config.margin
		)
		.attr('id', 'drawing')

	goban(canvas, config, config.size)
}

function firstLoad() {
	document.getElementById('noscript').hidden = true
	render()
}

if (document.readyState === "loading") {
	document.addEventListener("DOMContentLoaded", firstLoad)
} else {
	firstLoad()
}

document.forms[0].onsubmit = (event) => {
	render()
	event.preventDefault()
}

document.forms[0].querySelectorAll('input')
	.forEach(input => input.onchange = render)

let last_download
document.getElementById('download').onclick = (event) => {
	if (last_download) {
		URL.revokeObjectURL(last_download)
	}
	const canvas = SVG('#drawing')
	if (!canvas) {
		throw Error("No drawing to export")
	}
	console.log(canvas.svg())
	const a = document.createElement('a')
	a.href = URL.createObjectURL(new Blob([canvas.svg()]))
	a.download = document.getElementById('draw-container').dataset.filename
	last_download = a.href
	a.click()
}

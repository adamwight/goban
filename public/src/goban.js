function coord(config, x, y) {
  return {
    x: x * config.cell_width,
    y: y * config.cell_height
  }
}

function s_coord(config, x, y) {
  const c = coord(config, x, y)
  return `${c.x} ${c.y}`
}

function path_group(canvas, config, path) {
	canvas.group()
		.path(path.join('\n'))
		.fill('none')
		.stroke({width: config.line_width, color: '#000'})
		.attr('stroke-linecap', 'square')
		.back()
}

export function grid(canvas, config, count) {
  path_group(canvas, config,
    [...Array(count).keys()].flatMap(x => [
      `M ${s_coord(config, x, 0)}`,
      `L ${s_coord(config, x, count - 1)}`
    ])
  )

  path_group(canvas, config,
    [...Array(count).keys()].flatMap(y => [
      `M ${s_coord(config, 0, y)}`,
      `L ${s_coord(config, count - 1, y)}`
    ])
  )
}

export function star_points(canvas, config, positions) {
	const group = canvas.group()
	positions.forEach(([x, y]) => {
    const c = coord(config, x - 1, y - 1)
		group.circle(config.star_point_diameter).fill('#000').center(c.x, c.y)
  })
}

export function goban(canvas, config, size) {
	grid(canvas, config, size)

	switch (size) {
		case 9:
			star_points(canvas, config, [[3, 3], [3, 7], [7, 7], [7, 3], [5, 5]])
			break
		case 13:
			star_points(canvas, config, [[4, 4], [4, 10], [10, 10], [10, 4], [7, 7]])
			break
		case 17:
			break
		case 19:
			star_points(canvas, config, [[4, 4], [4, 10], [4, 16], [10, 4], [10, 10], [10, 16], [16, 4], [16, 10], [16, 16]])
			break
		default:
			throw Error(`Unknown board size '${size}'`)
	}
}
